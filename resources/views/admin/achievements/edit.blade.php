@extends('layouts.metronic')
@section('title', 'Edit Prestasi Siswa')

@section('content')
    <!--begin::Content wrapper-->
    <div class="d-flex flex-column flex-column-fluid">
        <!--begin::Toolbar-->
        <div id="kt_app_toolbar" class="app-toolbar py-3 py-lg-6">
            <!--begin::Toolbar container-->
            <div id="kt_app_toolbar_container" class="app-container container-fluid d-flex flex-stack">
                <!--begin::Page title-->
                <div class="page-title d-flex flex-column justify-content-center flex-wrap me-3">
                    <!--begin::Title-->
                    <h1 class="page-heading d-flex text-dark fw-bold fs-3 flex-column justify-content-center my-0">
                        Edit Prestasi Siswa</h1>
                    <!--end::Title-->
                    <!--begin::Breadcrumb-->
                    <ul class="breadcrumb breadcrumb-separatorless fw-semibold fs-7 my-0 pt-1">
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('admin.dashboard') }}" class="text-muted text-hover-primary">Dasbor</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">
                            <a href="{{ route('admin.achievements.index') }}" class="text-muted text-hover-primary">Prestasi
                                Siswa</a>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item">
                            <span class="bullet bg-gray-400 w-5px h-2px"></span>
                        </li>
                        <!--end::Item-->
                        <!--begin::Item-->
                        <li class="breadcrumb-item text-muted">Edit Prestasi Siswa</li>
                        <!--end::Item-->
                    </ul>
                    <!--end::Breadcrumb-->
                </div>
                <!--end::Page title-->
                <!--begin::Actions-->
                <div class="d-flex align-items-center gap-2 gap-lg-3">
                    <!--begin::Primary button-->
                    <a href="{{ route('admin.achievements.index') }}" class="btn btn-sm fw-bold btn-primary">
                        Kembali
                    </a>
                    <!--end::Primary button-->
                </div>
                <!--end::Actions-->
            </div>
            <!--end::Toolbar container-->
        </div>
        <!--end::Toolbar-->
        <!--begin::Content-->
        <div id="kt_app_content" class="app-content flex-column-fluid">
            <!--begin::Content container-->
            <div id="kt_app_content_container" class="app-container container-fluid">
                <!--begin::Row-->
                <div class="row g-5 g-xl-10 mb-5 mb-xl-10">
                    <!--begin::Col-->
                    <div class="col-12">
                        <form action="{{ route('admin.achievements.update', $achievement) }}" method="POST"
                            enctype="multipart/form-data">
                            @csrf
                            @method('PUT')

                            <!--begin::Tables widget 14-->
                            <div class="card">
                                <!--begin::Card header-->
                                <div class="card-header border-0">
                                    <!--begin::Card title-->
                                    <div class="card-title m-0">
                                        <h3 class="fw-bold m-0">Edit Prestasi Siswa</h3>
                                    </div>
                                    <!--end::Card title-->
                                </div>
                                <!--begin::Card header-->
                                <!--begin::Body-->
                                <div class="card-body pt-6 border-top">
                                    @if ($errors->any())
                                        <ul>
                                            @foreach ($errors->all() as $error)
                                                <li>{{ $error }}</li>
                                            @endforeach
                                        </ul>
                                    @endif

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Nama Perlombaan</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" name="title" value="{{ old('title', $achievement->title) }}"
                                            class="form-control @error('title') is-invalid @enderror">
                                        <!--end::Input-->

                                        @error('title')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Prestasi</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="text" name="achievement"
                                            value="{{ old('achievement', $achievement->achievement) }}"
                                            class="form-control @error('achievement') is-invalid @enderror">
                                        <!--end::Input-->

                                        @error('achievement')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->

                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <!--begin::Input group-->
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="fs-6 fw-semibold form-label mt-3">
                                                    <span>Kategori</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <select name="achievement_category_id"
                                                    class="form-control @error('achievement_category_id') is-invalid @enderror form-select"
                                                    data-control="select2">
                                                    @foreach ($categories as $category)
                                                        <option value="{{ $category->id }}"
                                                            @if (old('achievement_category_id', $achievement->achievement_category_id) == $category->id) selected @endif>
                                                            {{ $category->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <!--end::Input-->

                                                @error('achievement_category_id')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <!--end::Input group-->
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <!--begin::Input group-->
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="fs-6 fw-semibold form-label mt-3">
                                                    <span>Tingkat Perlombaan</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <select name="championship_level_id"
                                                    class="form-control @error('championship_level_id') is-invalid @enderror form-select"
                                                    data-control="select2">
                                                    @foreach ($levels as $level)
                                                        <option value="{{ $level->id }}"
                                                            @if (old('championship_level_id', $achievement->championship_level_id) == $level->id) selected @endif>
                                                            {{ $level->name }}
                                                        </option>
                                                    @endforeach
                                                </select>
                                                <!--end::Input-->

                                                @error('championship_level_id')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                            <!--end::Input group-->
                                        </div>
                                    </div>

                                    <!--begin::Input group-->
                                    <div class="row">
                                        <div class="col-12 col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="fs-6 fw-semibold form-label mt-3">
                                                    <span>Penyelenggara</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <input type="text" name="organizer"
                                                    value="{{ old('organizer', $achievement->organizer) }}"
                                                    class="form-control @error('organizer') is-invalid @enderror">
                                                <!--end::Input-->

                                                @error('organizer')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                        <div class="col-12 col-md-6">
                                            <div class="fv-row mb-7">
                                                <!--begin::Label-->
                                                <label class="fs-6 fw-semibold form-label mt-3">
                                                    <span>Tahun</span>
                                                </label>
                                                <!--end::Label-->
                                                <!--begin::Input-->
                                                <input type="number" name="year"
                                                    value="{{ old('year', $achievement->year) }}"
                                                    class="form-control @error('year') is-invalid @enderror">
                                                <!--end::Input-->

                                                @error('year')
                                                    <div class="invalid-feedback">
                                                        {{ $message }}
                                                    </div>
                                                @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Foto</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <input type="file" name="picture"
                                            class="form-control @error('picture') is-invalid @enderror">
                                        <!--end::Input-->

                                        @error('picture')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->

                                    <!--begin::Input group-->
                                    <div class="fv-row mb-7">
                                        <!--begin::Label-->
                                        <label class="fs-6 fw-semibold form-label mt-3">
                                            <span>Status Peserta</span>
                                        </label>
                                        <!--end::Label-->
                                        <!--begin::Input-->
                                        <div class="form-group">
                                            <div class="form-check form-check mb-3">
                                                <input type="radio" name="status" id="VALUE-1" value="individual"
                                                    class="form-check-input"
                                                    @if (old('status', $achievement->status) == 'individual') checked @endif>
                                                <label class="form-check-label" for="VALUE-1">Individu</label>
                                            </div>
                                            <div class="form-check form-check">
                                                <inpuT type="radio" name="status" id="VALUE-2" value="group"
                                                    class="form-check-input"
                                                    @if (old('status', $achievement->status) == 'group') checked @endif>
                                                <label class="form-check-label" for="VALUE-2">Kelompok</label>
                                            </div>
                                        </div>
                                        <!--end::Input-->

                                        @error('status')
                                            <div class="invalid-feedback">
                                                {{ $message }}
                                            </div>
                                        @enderror
                                    </div>
                                    <!--end::Input group-->

                                    <div class="students">
                                        <!--begin::Input group-->
                                        <div class="fv-row">
                                            <!--begin::Label-->
                                            <div class="d-flex justify-content-between">
                                                <label class="fs-4 fw-semibold form-label mt-3">
                                                    <span>Siswa</span>
                                                </label>

                                                <button type="button" class="btn btn-sm btn-primary add-student">
                                                    <i class="fas fa-plus"></i>
                                                </button>
                                            </div>
                                            <!--end::Label-->
                                        </div>

                                        @if (old('students'))
                                            @foreach (old('students') as $student)
                                                <div class="row student-field">
                                                    <div class="col-md-6 col-12">
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-7">
                                                            <!--begin::Label-->
                                                            <label class="fs-6 fw-semibold form-label mt-3">
                                                                <span>Siswa</span>
                                                            </label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <select name="students[{{ $loop->index }}][id]"
                                                                class="form-control @error('student') is-invalid @enderror">
                                                                @foreach ($students as $item)
                                                                    <option value="{{ $item->id }}"
                                                                        @if ($student['id'] == $item->id) selected @endif>
                                                                        {{ $item->user->name }} ({{ $item->nisn }})
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            <!--end::Input-->

                                                            @error('students.0.id')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-7">
                                                            <!--begin::Label-->
                                                            <label class="fs-6 fw-semibold form-label mt-3">
                                                                <span>Sertifikat</span>
                                                            </label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <input type="file" name="students[0][certificate]"
                                                                class="form-control @error('students.0.certificate') is-invalid @enderror">
                                                            <!--end::Input-->

                                                            @error('students.0.certificate')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                </div>
                                            @endforeach
                                        @else
                                            @foreach ($achievement->students as $item)
                                                <div class="row student-field">
                                                    <div class="col-md-6 col-12">
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-7">
                                                            <!--begin::Label-->
                                                            <label class="fs-6 fw-semibold form-label mt-3">
                                                                <span>Siswa</span>
                                                            </label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <select name="students[{{ $loop->index }}][id]"
                                                                class="form-control @error('student') is-invalid @enderror">
                                                                @foreach ($students as $student)
                                                                    <option value="{{ $student->id }}"
                                                                        @if ($student->id == $item->id) selected @endif>
                                                                        {{ $student->user->name }} ({{ $student->nisn }})
                                                                    </option>
                                                                @endforeach
                                                            </select>
                                                            <!--end::Input-->

                                                            @error('students.{{ $loop->index }}.id')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                    <div class="col-md-6 col-12">
                                                        <!--begin::Input group-->
                                                        <div class="fv-row mb-7">
                                                            <!--begin::Label-->
                                                            <label class="fs-6 fw-semibold form-label mt-3">
                                                                <span>Sertifikat</span>
                                                            </label>
                                                            <!--end::Label-->
                                                            <!--begin::Input-->
                                                            <input type="file" name="students[{{ $loop->index }}][certificate]"
                                                                class="form-control @error('students.{{ $loop->index }}.certificate') is-invalid @enderror">
                                                            <!--end::Input-->

                                                            @error('students.0.certificate')
                                                                <div class="invalid-feedback">
                                                                    {{ $message }}
                                                                </div>
                                                            @enderror
                                                        </div>
                                                        <!--end::Input group-->
                                                    </div>
                                                </div>
                                            @endforeach
                                        @endif

                                        <div class="new-students"></div>
                                    </div>
                                </div>
                                <!--end: Card Body-->
                                <!--begin::Footer-->
                                <div class="card-footer d-flex justify-content-end py-6 px-9">
                                    <button type="submit" class="btn btn-primary"
                                        id="kt_forms_widget_14_submit_button">Simpan</button>
                                </div>
                                <!--end::Footer-->
                            </div>
                            <!--end::Tables widget 14-->
                        </form>
                    </div>
                    <!--end::Col-->
                </div>
                <!--end::Row-->
            </div>
            <!--end::Content container-->
        </div>
        <!--end::Content-->
    </div>
    <!--end::Content wrapper-->
@endsection

@push('custom_js')
    <script>
        $(document).ready(function() {
            var index = $('.student-field').length;

            $('.add-student').on('click', function() {
                var html = `
                    <div class="row">
                        <div class="col-md-6 col-12">
                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Siswa</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <select name="students[${index}][id]"
                                    class="form-control @error('student') is-invalid @enderror form-select"
                                    data-control="select2">
                                    @foreach ($students as $student)
                                        <option value="{{ $student->id }}"
                                            @if (old('DROPDOWN_FIELD') == $student->id) selected @endif>
                                            {{ $student->user->name }} ({{ $student->nisn }})
                                        </option>
                                    @endforeach
                                </select>
                                <!--end::Input-->

                                @error('students.${index}.id')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->
                        </div>
                        <div class="col-md-6 col-12">
                            <!--begin::Input group-->
                            <div class="fv-row mb-7">
                                <!--begin::Label-->
                                <label class="fs-6 fw-semibold form-label mt-3">
                                    <span>Sertifikat</span>
                                </label>
                                <!--end::Label-->
                                <!--begin::Input-->
                                <input type="file" name="students[${index}][certificate]"
                                    class="form-control">
                                <!--end::Input-->

                                @error('students.${index}.certificate')
                                    <div class="invalid-feedback">
                                        {{ $message }}
                                    </div>
                                @enderror
                            </div>
                            <!--end::Input group-->
                        </div>
                    </div>
                `;

                $('.new-students').append(html);
                index++;
            });
        });
    </script>
@endpush
