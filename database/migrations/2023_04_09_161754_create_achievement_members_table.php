<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementMembersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievement_members', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Achievement::class)->nullable()->constrained()->cascadeOnDelete();
            $table->foreignIdFor(\App\Models\Student::class)->nullable()->constrained()->cascadeOnDelete();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievement_members');
    }
}
