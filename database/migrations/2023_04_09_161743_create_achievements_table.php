<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAchievementsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('achievements', function (Blueprint $table) {
            $table->id();
            $table->foreignIdFor(\App\Models\Achievement_category::class)->nullable()->constrained()->nullOnDelete();
            $table->foreignIdFor(\App\Models\Championship_level::class)->nullable()->constrained()->nullOnDelete();
            $table->string('title');
            $table->string('slug');
            $table->year('year')->nullable();
            $table->string('organizer')->nullable();
            $table->string('achievement')->nullable();
            $table->enum('status', ['individual', 'group'])->default('individual');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('achievements');
    }
}
