<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\NewsController;
use App\Http\Controllers\Admin\StudentController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\AchievementController;
use App\Http\Controllers\ChampionshipLevelController;
use App\Http\Controllers\Admin\NewsCategoryController;
use App\Http\Controllers\Admin\AchievementCategoryController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'middleware' => ['role:admin']], function () {
    //semua route di dalam sini hanya bisa diakses "admin"

    Route::get('/dashboard', [DashboardController::class, 'index'])->name('dashboard');
    Route::resource('achievement-categories', AchievementCategoryController::class);
    Route::resource('news-categories', NewsCategoryController::class);
    Route::resource('championship-levels', ChampionshipLevelController::class);
    Route::resource('students', StudentController::class);
    Route::resource('news', NewsController::class);
    Route::resource('achievements', AchievementController::class);
});

Route::group(['prefix' => 'student', 'as' => 'student.', 'middleware' => ['role:student']], function () {
    //semua route di dalam sini hanya bisa diakses "student"
});



require __DIR__ . '/auth.php';
