<?php

namespace App\Http\Controllers;

use App\Models\Championship_level;
use Illuminate\Http\Request;

class ChampionshipLevelController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $levels = Championship_level::paginate();

        return view('admin.championship-levels.index', compact('levels'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.championship-levels.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:championship_levels,name',
        ]);

        $level = new Championship_level();
        $level->name = $request->name;
        $level->save();

        return redirect()
            ->route('admin.championship-levels.index')
            ->withSuccess('Berhasil menambah level kejuaraan');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Championship_level  $championship_level
     * @return \Illuminate\Http\Response
     */
    public function show(Championship_level $championship_level)
    {
        return view('admin.championship-levels.show', compact('championship_level'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Championship_level  $championship_level
     * @return \Illuminate\Http\Response
     */
    public function edit(Championship_level $championship_level)
    {
        return view('admin.championship-levels.edit', compact('championship_level'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Championship_level  $championship_level
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Championship_level $championship_level)
    {
        $request->validate([
            'name' => 'required|unique:championship_levels,name,' . $championship_level->id,
        ]);

        $championship_level->name = $request->name;
        $championship_level->save();

        return redirect()
            ->route('admin.championship-levels.index')
            ->withSuccess('Berhasil mengubah level kejuaraan');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Championship_level  $championship_level
     * @return \Illuminate\Http\Response
     */
    public function destroy(Championship_level $championship_level)
    {
        $championship_level->delete();

        return redirect()
            ->route('admin.championship-levels.index')
            ->withSuccess('Berhasil menghapus level kejuaraan');
    }
}
