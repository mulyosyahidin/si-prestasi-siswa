<?php

namespace App\Http\Controllers\Admin;

use App\Models\News;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\News_category;
use App\Http\Controllers\Controller;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $news = News::paginate();

        return view('admin.news.index', compact('news'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = News_category::all();

        return view('admin.news.create', compact('categories'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'title' => 'required',
            'news_category_id' => 'required|numeric',
            'content' => 'required',
            'picture' => 'nullable',
            'status' => 'required',
        ]);

        $news = new News();
        $news->user_id = auth()->user()->id;
        $news->title = $request->title;
        $news->slug = Str::slug($request->title);
        $news->news_category_id = $request->news_category_id;
        $news->content = $request->content;
        $news->status = $request->status;
        $news->save();

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $news->addMedia($request->picture)
                ->toMediaCollection('news_pictures');
        }

        return redirect()
            ->route('admin.news.show', $news)
            ->withSuccess('Berhasil menambah berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function show(News $news)
    {
        return view('admin.news.show', compact('news'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function edit(News $news)
    {
        $categories = News_category::all();

        return view('admin.news.edit', compact('news', 'categories'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News $news)
    {
        $request->validate([
            'title' => 'required',
            'news_category_id' => 'required|numeric',
            'content' => 'required',
            'picture' => 'nullable',
            'status' => 'required',
        ]);

        $news->title = $request->title;
        $news->news_category_id = $request->news_category_id;
        $news->content = $request->content;
        $news->status = $request->status;
        $news->save();

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $news->clearMediaCollection('news_pictures');
            $news->addMedia($request->picture)
                ->toMediaCollection('news_pictures');
        }

        return redirect()
            ->route('admin.news.show', $news)
            ->withSuccess('Berhasil mengubah berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News  $news
     * @return \Illuminate\Http\Response
     */
    public function destroy(News $news)
    {
        $news->clearMediaCollection('news_pictures');
        $news->delete();

        return redirect()
            ->route('admin.news.index')
            ->withSuccess('Berhasil menghapus berita');
    }
}
