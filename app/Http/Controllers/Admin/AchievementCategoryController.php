<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Achievement_category;
use Illuminate\Http\Request;

class AchievementCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = Achievement_category::paginate(10);

        return view('admin.achievement-categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.achievement-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //(1) validasi
        $request->validate([
            'name' => 'required|unique:achievement_categories,name',
        ]);

        //(2) simpan data
        $category = new Achievement_category();
        $category->name = $request->name;

        $category->save();

        //(3) redirect
        return redirect()
            ->route('admin.achievement-categories.index')
            ->withSuccess('Berhasil menambah kategori');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Achievement_category  $achievement_category
     * @return \Illuminate\Http\Response
     */
    public function show(Achievement_category $achievement_category)
    {
        return view('admin.achievement-categories.show', compact('achievement_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Achievement_category  $achievement_category
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement_category $achievement_category)
    {
        return view('admin.achievement-categories.edit', compact('achievement_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Achievement_category  $achievement_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Achievement_category $achievement_category)
    {
        $request->validate([
            'name' => 'required|unique:achievement_categories,name,' . $achievement_category->id,
        ]);

        $achievement_category->name = $request->name;
        $achievement_category->save();

        return redirect()
            ->route('admin.achievement-categories.show', $achievement_category)
            ->withSuccess('Berhasil memperbarui data');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Achievement_category  $achievement_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement_category $achievement_category)
    {
        $achievement_category->delete();

        return redirect()
            ->route('admin.achievement-categories.index')
            ->withSuccess('Berhasil menghapus kategori prestasi');
    }
}
