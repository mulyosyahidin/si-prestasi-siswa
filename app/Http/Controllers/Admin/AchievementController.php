<?php

namespace App\Http\Controllers\Admin;

use App\Models\Student;
use App\Models\Achievement;
use Illuminate\Support\Str;
use Illuminate\Http\Request;
use App\Models\Championship_level;
use App\Http\Controllers\Controller;
use App\Models\Achievement_category;
use App\Models\Achievement_member;

class AchievementController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $achievements = Achievement::paginate();

        return view('admin.achievements.index', compact('achievements'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categories = Achievement_category::all();
        $levels = Championship_level::all();
        $students = Student::all();

        return view('admin.achievements.create', compact('categories', 'levels', 'students'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate title, achievement, achievement_category_id, championship_level_id, year, organizer, status, students
        $request->validate([
            'title' => 'required',
            'achievement' => 'required',
            'achievement_category_id' => 'required|numeric',
            'championship_level_id' => 'required|numeric',
            'year' => 'required',
            'organizer' => 'required',
            'status' => 'required',
            'students' => 'required',
            'picture' => 'nullable',
            'students.*.id' => 'required|numeric',
            'students.*.certificate' => 'nullable|mimes:pdf,jpg,jpeg,png'
        ], [
            'students.*.certificate.mimes' => 'Sertifikat harus file PDF atau Gambar'
        ]);

        $achievement = new Achievement();
        $achievement->achievement_category_id = $request->achievement_category_id;
        $achievement->championship_level_id = $request->championship_level_id;
        $achievement->title = $request->title;
        $achievement->slug = Str::slug($request->title);
        $achievement->achievement = $request->achievement;
        $achievement->year = $request->year;
        $achievement->organizer = $request->organizer;
        $achievement->status = $request->status;
        $achievement->save();

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $achievement->addMediaFromRequest('picture')
                ->toMediaCollection('achievement_pictures');
        }

        $students = $request->students;

        if (count($students) > 0) {
            foreach ($students as $student) {
                $member = new Achievement_member();
                $member->achievement_id = $achievement->id;
                $member->student_id = $student['id'];
                $member->save();

                if (isset($student['certificate'])) {
                    $member->addMedia($student['certificate'])
                        ->toMediaCollection('achievement_certificates');
                }
            }
        }

        return redirect()
            ->route('admin.achievements.show', $achievement)
            ->withSuccess('Berhasil menambah data prestasi siswa');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function show(Achievement $achievement)
    {
        $students = Achievement_member::where('achievement_id', $achievement->id)->get();

        return view('admin.achievements.show', compact('achievement', 'students'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function edit(Achievement $achievement)
    {
        $categories = Achievement_category::all();
        $levels = Championship_level::all();
        $students = Student::all();

        return view('admin.achievements.edit', compact('achievement', 'categories', 'levels', 'students'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Achievement $achievement)
    {
        //validate title, achievement, achievement_category_id, championship_level_id, year, organizer, status, students
        $request->validate([
            'title' => 'required',
            'achievement' => 'required',
            'achievement_category_id' => 'required|numeric',
            'championship_level_id' => 'required|numeric',
            'year' => 'required',
            'organizer' => 'required',
            'status' => 'required',
            'students' => 'required',
            'picture' => 'nullable',
            'students.*.id' => 'required|numeric',
            'students.*.certificate' => 'nullable|mimes:pdf,jpg,jpeg,png'
        ], [
            'students.*.certificate.mimes' => 'Sertifikat harus file PDF atau Gambar'
        ]);

        $achievement->achievement_category_id = $request->achievement_category_id;
        $achievement->championship_level_id = $request->championship_level_id;
        $achievement->title = $request->title;
        $achievement->slug = Str::slug($request->title);
        $achievement->achievement = $request->achievement;
        $achievement->year = $request->year;
        $achievement->organizer = $request->organizer;
        $achievement->status = $request->status;
        $achievement->save();

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $achievement->clearMediaCollection('achievement_pictures');
            $achievement->addMediaFromRequest('picture')
                ->toMediaCollection('achievement_pictures');
        }

        $students = $request->students;

        if (count($students) > 0) {
            foreach ($students as $student) {
                $member = Achievement_member::where('achievement_id', $achievement->id)
                    ->where('student_id', $student['id'])
                    ->first();

                if ($member) {
                    if (isset($student['certificate'])) {
                        $member->clearMediaCollection('achievement_certificates');


                        $member->addMedia($student['certificate'])
                            ->toMediaCollection('achievement_certificates');
                    }
                } else {
                    $member = new Achievement_member();
                    $member->achievement_id = $achievement->id;
                    $member->student_id = $student['id'];
                    $member->save();

                    if (isset($student['certificate'])) {
                        $member->addMedia($student['certificate'])
                            ->toMediaCollection('achievement_certificates');
                    }
                }
            }
        }

        return redirect()
            ->route('admin.achievements.show', $achievement)
            ->withSuccess('Berhasil mengubah data prestasi siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Achievement  $achievement
     * @return \Illuminate\Http\Response
     */
    public function destroy(Achievement $achievement)
    {
        $achievement->clearMediaCollection('achievement_pictures');

        $members = Achievement_member::where('achievement_id', $achievement->id)->get();

        if (count($members) > 0) {
            foreach ($members as $member) {
                $member->clearMediaCollection('achievement_certificates');
            }
        }

        $achievement->delete();

        return redirect()
            ->route('admin.achievements.index')
            ->withSuccess('Berhasil menghapus data prestasi');
    }
}
