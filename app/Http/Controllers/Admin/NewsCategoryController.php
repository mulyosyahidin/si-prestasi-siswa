<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\News_category;
use Illuminate\Http\Request;

class NewsCategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $categories = News_category::paginate();

        return view('admin.news-categories.index', compact('categories'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.news-categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:news_categories,name',
        ]);

        $category = new News_category();
        $category->name = $request->name;
        $category->save();

        return redirect()
            ->route('admin.news-categories.index')
            ->withSuccess('Berhasil menambah kategori berita');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\News_category  $news_category
     * @return \Illuminate\Http\Response
     */
    public function show(News_category $news_category)
    {
        return view('admin.news-categories.show', compact('news_category'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\News_category  $news_category
     * @return \Illuminate\Http\Response
     */
    public function edit(News_category $news_category)
    {
        return view('admin.news-categories.edit', compact('news_category'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\News_category  $news_category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, News_category $news_category)
    {
        $request->validate([
            'name' => 'required|unique:news_categories,name,' . $news_category->id,
        ]);

        $news_category->name = $request->name;
        $news_category->save();

        return redirect()
            ->route('admin.news-categories.show', $news_category)
            ->withSuccess('Berhasil mengubah kategori berita');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\News_category  $news_category
     * @return \Illuminate\Http\Response
     */
    public function destroy(News_category $news_category)
    {
        $news_category->delete();

        return redirect()
            ->route('admin.news-categories.index')
            ->withSuccess('Berhasil menghapus kategori berita');
    }
}
