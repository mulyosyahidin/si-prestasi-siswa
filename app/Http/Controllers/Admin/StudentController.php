<?php

namespace App\Http\Controllers\Admin;

use App\Models\User;
use App\Models\Student;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = Student::paginate();

        return view('admin.students.index', compact('students'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.students.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //validate name, email, password, nisn, phone_number, address, picture
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|unique:users|max:255',
            'password' => 'required|min:8',
            'nisn' => 'required|numeric|unique:students,nisn',
            'phone_number' => 'nullable|numeric',
            'address' => 'nullable',
            'picture' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //create user
        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);

        //create student
        $user->student()->create([
            'nisn' => $request->nisn,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
        ]);

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $user->addMediaFromRequest('picture')
                ->toMediaCollection('user_pictures');
        }

        return redirect()
            ->route('admin.students.index')
            ->withSuccess('Berhasil menambah siswa baru');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Student $student)
    {
        return view('admin.students.show', compact('student'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        return view('admin.students.edit', compact('student'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        //validate name, email, password, nisn, phone_number, address, picture
        $request->validate([
            'name' => 'required',
            'email' => 'required|email|max:255|unique:users,email,' . $student->user->id,
            'password' => 'nullable|min:8',
            'nisn' => 'required|numeric|unique:students,nisn,' . $student->id,
            'phone_number' => 'nullable|numeric',
            'address' => 'nullable',
            'picture' => 'nullable|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        //update user
        $student->user->update([
            'name' => $request->name,
            'email' => $request->email,
            'password' => $request->password ? Hash::make($request->password) : $student->user->password,
        ]);

        //update student
        $student->update([
            'nisn' => $request->nisn,
            'phone_number' => $request->phone_number,
            'address' => $request->address,
        ]);

        if ($request->hasFile('picture') && $request->file('picture')->isValid()) {
            $student->user->clearMediaCollection('user_pictures');

            $student->user->addMediaFromRequest('picture')
                ->toMediaCollection('user_pictures');
        }

        return redirect()
            ->route('admin.students.index')
            ->withSuccess('Berhasil mengubah data siswa');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        $student->user->clearMediaCollection('user_pictures');
        $student->user->delete();
        $student->delete();

        return redirect()
            ->route('admin.students.index')
            ->withSuccess('Berhasil menghapus siswa');
    }
}
