<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\HasMedia;
use Spatie\MediaLibrary\InteractsWithMedia;

class Achievement extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    public function category()
    {
        return $this->belongsTo(Achievement_category::class, 'achievement_category_id');
    }

    public function level()
    {
        return $this->belongsTo(Championship_level::class, 'championship_level_id');
    }

    public function students()
    {
        return $this->belongsToMany(Student::class, 'achievement_members');
    }
}
