<?php

namespace App\Models;

use Spatie\MediaLibrary\HasMedia;
use Illuminate\Database\Eloquent\Model;
use Spatie\MediaLibrary\InteractsWithMedia;
use Illuminate\Database\Eloquent\Factories\HasFactory;

class Achievement_member extends Model implements HasMedia
{
    use HasFactory, InteractsWithMedia;

    public $timestamps = false;

    protected $fillable = [
        'achievement_id',
        'student_id',
    ];

    public function achievement()
    {
        return $this->belongsTo(Achievement::class);
    }

    public function student()
    {
        return $this->belongsTo(Student::class, 'student_id');
    }
}
